import {FormControl} from '@angular/forms';

export interface CrudView<E> {

  findAll();
  find(id: number);
  saveOrUpdate(form: FormControl);
  save(entity: E);
  update(entity: E);
  updateTitle();


}
