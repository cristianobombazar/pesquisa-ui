import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyCadastroComponent } from './company-cadastro/company-cadastro.component';
import {FormsModule} from '@angular/forms';
import {CalendarModule, DropdownModule, InputMaskModule, InputTextModule, SelectButtonModule, TabViewModule} from 'primeng/primeng';
import {CountiesService} from '../../services/cadastro/counties.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {CardModule} from 'primeng/card';
import {CompanyService} from '../../services/cadastro/company.service';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    InputTextModule,
    SelectButtonModule,
    InputMaskModule,
    CalendarModule,
    TabViewModule,
    DropdownModule,
    CardModule,
    SelectButtonModule
  ],
  declarations: [
    CompanyCadastroComponent
  ],
  exports: [
    CompanyCadastroComponent
  ]
})
export class CadastroModule {
}
