import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Company} from '../../../models/cadastro/company.model';
import {CrudView} from '../../crud-view.model';
import {CountiesService} from '../../../services/cadastro/counties.service';
import {County} from '../../../models/cadastro/county.model';
import {CompanyService} from '../../../services/cadastro/company.service';

@Component({
  selector: 'app-company-cadastro',
  templateUrl: './company-cadastro.component.html',
  styleUrls: ['./company-cadastro.component.css']
})
export class CompanyCadastroComponent implements OnInit, CrudView<Company> {

  company = new Company();
  title: String;
  counties: any;

  tipos = [
    { label: 'Ativo', value: true },
    { label: 'Inativo', value: false },
  ];

  constructor(private countyService: CountiesService, private service: CompanyService
  ) { }

  ngOnInit() {
    if (this.company.id) {
      this.title = 'Editar ';
    } else {
      this.title = 'Nova ';
      this.company.name = 'Cristiano Bombazar'
      this.company.inscription = '000000000000000';
      this.company.address.street = 'Rua Jacó Martins de Souza';
      this.company.address.number = '69';
      this.company.address.district = 'São Basílio';
      this.company.address.cep = '887500000';
      this.company.dateAdded = new Date();
      this.company.dateUpdate = new Date();
    }
    this.findAllCounties();
  }

  find(id: number) {
  }

  findAll() {
  }

  save(entity: Company) {
    if (entity) {
      this.service.save(entity);
    }
  }

  saveOrUpdate(form: FormControl) {
    if (this.company.id) {
      this.update(this.company);
    } else {
      this.save(this.company);
    }
  }

  update(entity: Company) {
    if (entity && entity.id) {
      this.service.update(entity);
    }
  }

  updateTitle() {
  }

  findAllCounties() {
    this.countyService.findAll(false).then(response => {
      this.counties = response.map( county => (
        {label: county.name, value: county.id}
      ));
    });
  }

  cancel() {
  }
}
