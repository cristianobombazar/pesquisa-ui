export interface CrudService<E> {
    findAll(paginate: boolean): Promise<Array<E>>;
    find(id: number): Promise<E>;
    save(body: E): Promise<E>;
    update(body: E): Promise<E>;
}
