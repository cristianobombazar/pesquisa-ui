import {Injectable } from '@angular/core';

import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

import {CrudService} from '../crud-service.model';
import {County} from '../../models/cadastro/county.model';
import {ErrorHandlerService} from '../../core/error-handler.service';

@Injectable()
export class CountiesService implements CrudService<County> {

  URI: string;
  URI_PAGINATE: string;

  constructor(public http: HttpClient,
              private errorService: ErrorHandlerService) {
    this.URI = environment.URI + '/county';
    this.URI_PAGINATE = environment.URI + '/county/';
  }

  find(id: number): Promise<County> {
    return this.http.get<County>(this.URI + '/' + id).toPromise().then( (response: any) => {
      return response;
    }).catch( error => this.errorService.handle(error));
  }

  findAll(paginate: boolean): Promise<Array<County>> {
    return this.http.get<Array<County>> (paginate ? this.URI_PAGINATE : this.URI).toPromise().then( (response: any) => {
        if (paginate) {
          return response.content;
        } else {
          return response;
        }
    }).catch( error => this.errorService.handle(error));
  }

  save(body: County): Promise<County> {
    return this.http.post<County>(this.URI, JSON.stringify(body)).toPromise().then( (response: any) => {
      console.log(response);
      return response;
    }).catch( error => this.errorService.handle(error));
  }

  update(body: County): Promise<County> {
    return this.http.put<County>(this.URI + '/' + body.id, JSON.stringify(body)).toPromise().then( (response: any) => {
      return response;
    }).catch( error => this.errorService.handle(error));
  }


}
