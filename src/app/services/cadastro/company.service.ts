import { Injectable } from '@angular/core';
import {CrudService} from '../crud-service.model';
import {Company} from '../../models/cadastro/company.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {County} from '../../models/cadastro/county.model';
import {ErrorHandlerService} from '../../core/error-handler.service';

@Injectable()
export class CompanyService implements CrudService<Company> {

  URI: string;
  URI_PAGINATE: string;

  constructor(private http: HttpClient,
              private errorService: ErrorHandlerService) {
    this.URI = environment.URI + '/company';
    this.URI_PAGINATE = environment.URI + '/company/';
  }

  find(id: number): Promise<Company> {
    return this.http.get<Company>(this.URI + '/' + id).toPromise();
  }

  findAll(paginate: boolean): Promise<Array<Company>> {
    return this.http.get<Array<Company>> (paginate ? this.URI_PAGINATE : this.URI).toPromise().then( (response: any) => {
      if (paginate) {
        return response.content;
      } else {
        return response;
      }
    }).catch( error => this.errorService.handle(error));
  }

  save(body: Company): Promise<Company> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post<Company>(this.URI, JSON.stringify(body), {headers}).toPromise().then( (response: any) => {
      return response;
    }).catch( error => this.errorService.handle(error));
  }

  update(body: Company): Promise<Company> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.put<Company>(this.URI + '/' + body.id, JSON.stringify(body), {headers}).toPromise().then( (response: any) => {
        return response;
    }).catch( error => this.errorService.handle(error));
  }

}
