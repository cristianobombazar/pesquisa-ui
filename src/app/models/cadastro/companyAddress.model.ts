import {Company} from './company.model';
import {County} from './county.model';

export class CompanyAddress {

  id: number;
  company: Company;
  street: string;
  number: string;
  cep: string;
  district: string;
  complement: string;
  county = new County();
}
