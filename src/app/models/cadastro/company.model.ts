import {CompanyAddress} from './companyAddress.model';

export class Company {
  id: number;
  name: string;
  inscription: string;
  dateAdded: Date;
  dateUpdate: Date;
  active: boolean;
  address = new CompanyAddress();
}
