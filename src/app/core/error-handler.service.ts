import { Injectable } from '@angular/core';
import {ToastyService} from 'ng2-toasty';
import {Response} from '@angular/http';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable()
export class ErrorHandlerService {

  constructor(
    private toastyService: ToastyService
  ) { }

  handle(errorResponse: any) {
    let msg: string;

    if (typeof errorResponse === 'string') {
      msg = errorResponse;
    } else if (errorResponse instanceof HttpErrorResponse && errorResponse.status >= 400 && errorResponse.status <= 499) {
      let errors;
      msg = 'Ocorreu um erro ao processar a sua solicitação';

      if (errorResponse.status === 403) {
        msg = 'Você não tem permissão para executar esta ação';
      }
      try {
        errors = errorResponse.error
        msg = errors[0].messageUser;
      } catch (e) { }
    } else {
      msg = 'Erro ao processar serviço remoto. Tente novamente.';
      console.error('Ocorreu um erro', errorResponse);
    }
    this.toastyService.error({
      title: 'Ocorreu um erro ao processar a requisição',
      msg,
      showClose: true,
      timeout: 3000
    });
  }

}
