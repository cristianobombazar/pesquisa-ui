import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ToastyModule} from 'ng2-toasty';
import {ErrorHandlerService} from './error-handler.service';
import {CompanyService} from '../services/cadastro/company.service';
import {CountiesService} from '../services/cadastro/counties.service';

@NgModule({
  imports: [
    CommonModule,
    ToastyModule.forRoot()
  ],
  declarations: [],
  exports: [
    ToastyModule
  ],
  providers: [
    ErrorHandlerService,
    CompanyService,
    CountiesService
  ]

})
export class CoreModule { }
